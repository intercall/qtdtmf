#-------------------------------------------------
#
# Project created by QtCreator 2010-08-08T22:43:58
#
#-------------------------------------------------

QT       -= gui

TARGET = QtDTMF
TEMPLATE = lib

DEFINES += QTDTMF_LIBRARY

SOURCES += qdtmf.cpp \
    main.cpp

HEADERS += qdtmf.h\
        QtDTMF_global.h
