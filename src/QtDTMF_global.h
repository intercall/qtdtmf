#ifndef QTDTMF_GLOBAL_H
#define QTDTMF_GLOBAL_H

#include <QtCore/qglobal.h>

#if defined(QTDTMF_LIBRARY)
#  define QTDTMFSHARED_EXPORT Q_DECL_EXPORT
#else
#  define QTDTMFSHARED_EXPORT Q_DECL_IMPORT
#endif

#endif // QTDTMF_GLOBAL_H
