#include <QFile>
#include <QDebug>
#include <QtCore/QCoreApplication>
#include <QtMultimedia/QAudioDeviceInfo>
#include <QtMultimedia/QAudioFormat>
#include <QtMultimedia/QAudioInput>
#include <QtMultimedia/QAudioOutput>
#include <math.h>

const int N = 160;
const int rate = 16000;

qreal detectFreq(qreal Freq, QByteArray *block)
{
    int k = (0.5+N*Freq/rate);
    qreal w = (qreal)(2*3.1415926)*k/N;
    qreal cosine = cos(w);
    qreal sine = sin(w);
    qreal coeff = 2*cosine;
    qreal Q0;
    qreal Q1 = 0;
    qreal Q2 = 0;
    for (int i = 0; i < block->length(); i++)
    {
        char d = block->at(i);

        Q0 = coeff * Q1 - Q2 + d;
        Q2 = Q1;
        Q1 = Q0;
    }
    qreal real = (Q1 - Q2 * cosine);
    qreal imag = (Q2 * sine);
    return real*real + imag*imag;
}

int low[] = {697, 770, 852, 941};
int high[] = {1209, 1336, 1477, 1633};
char results[] = {'1', '2', '3', 'A',
                  '4', '5', '6', 'B',
                  '7', '8', '9', 'C',
                  '*', '0', '#', 'D'};

QString detectDTMF(QFile *data)
{
    QString result;
    int sample = 0;
    QByteArray *block = new QByteArray();
    while (!data->atEnd())
    {
        char d;
        data->read(&d,1);
        block->append(d);

        sample++;
        if (sample >= N)
        {
            int imax = -1, jmax = -1;
            qreal mmax = 0;
            for (int i = 0; i < 4; i++)
            {
                for (int j = 0; j < 4; j++)
                {
                    qreal m = detectFreq(low[i], block) + detectFreq(high[j], block);
                    if (m >= mmax)
                    {
                        mmax = m;
                        imax = i;
                        jmax = j;
                    }
                }
            }
            if (imax >= 0 && jmax >= 0) result += results[imax*4+jmax]; else result += " ";
            sample = 0;
            block->clear();
        }
    }
    return result;
}

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    QAudioFormat _format;
    _format.setSampleRate(16000);
    _format.setChannelCount(1);
    _format.setSampleSize(8);
    _format.setCodec("audio/pcm");
    _format.setByteOrder(QAudioFormat::LittleEndian);
    _format.setSampleType(QAudioFormat::UnSignedInt);

    QAudioDeviceInfo::availableDevices(QAudio::AudioInput)[1].preferredFormat();
    if (!QAudioDeviceInfo::availableDevices(QAudio::AudioOutput)[1].isFormatSupported(_format))
    {
        qWarning()<<"default format not supported try to use nearest";
    }

    QAudioOutput *_output = new QAudioOutput(QAudioDeviceInfo::availableDevices(QAudio::AudioInput)[1], _format, NULL);

    QFile input("dtmf5.raw");
    input.open(QIODevice::ReadOnly);
    QFile output("DTMF5.raw");
    output.open(QIODevice::WriteOnly);
    short int data;
    while (!input.atEnd())
    {
        input.read((char *)&data,2);
        data += 65535/2;
        char d = (long int)data*255/65535;
        output.write(&d,1);
    }
    input.close();
    output.close();



    QFile *_outputFile = new QFile("DTMF5.raw");
    if (!_outputFile->open(QIODevice::ReadOnly)) qDebug() << "error";

    QString result = detectDTMF(_outputFile);
    qDebug() << result;

    //_output->start(_outputFile);

    //return a.exec();
}
